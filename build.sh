#!/bin/env bash

BIT_BIN="$HOME/bin/bit"
PORT=5000

# npm install --global @teambit/bvm
# bvm install

${BIT_BIN} init --harmony
${BIT_BIN} import
${BIT_BIN} install
${BIT_BIN} build
