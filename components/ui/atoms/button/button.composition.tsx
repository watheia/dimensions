import React from "react"
import Button from "."
import { Container, makeStyles } from "@material-ui/core"
import { TopLayout } from "@waweb/dimensions.ui.layout.top-layout"

const useStyles = makeStyles((theme) => ({
  container: {
    spacing: theme.spacing(1),
  },
}))

export const DefaultButtons = () => {
  const classes = useStyles()
  return (
    <TopLayout>
      <Container className={classes.container}>
        <button>Native</button>
        <Button variant="contained">Contained</Button>
        <Button variant="outlined">Outlined</Button>
        <Button variant="text">Text</Button>
        <Button disabled>Disabled</Button>
      </Container>
    </TopLayout>
  )
}

export const PrimaryButtons = () => {
  const classes = useStyles()
  return (
    <TopLayout>
      <Container className={classes.container}>
        <Button color="primary" variant="contained">
          Contained
        </Button>
        <Button color="primary" variant="outlined">
          Outlined
        </Button>
        <Button color="primary" variant="text">
          Text
        </Button>
        <Button color="primary" disabled>
          Disabled
        </Button>
      </Container>
    </TopLayout>
  )
}

export const SecondaryButtons = () => {
  const classes = useStyles()
  return (
    <TopLayout>
      <Container className={classes.container}>
        <Button color="secondary" variant="contained">
          Contained
        </Button>
        <Button color="secondary" variant="outlined">
          Outlined
        </Button>
        <Button color="secondary" variant="text">
          Text
        </Button>
        <Button color="secondary" disabled>
          Disabled
        </Button>
      </Container>
    </TopLayout>
  )
}

export const LinkButtons = () => {
  const classes = useStyles()
  return (
    <TopLayout>
      <Container className={classes.container}>
        <Button href="#">Default</Button>
        <Button href="#" color="primary">
          Primary
        </Button>
        <Button href="#" color="secondary">
          Secondary
        </Button>
        <Button href="#" disabled>
          Disabled
        </Button>
      </Container>
    </TopLayout>
  )
}
