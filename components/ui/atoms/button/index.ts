import { Button, ButtonProps } from "@material-ui/core"

export type { ButtonProps }
export { Button }

export default Button
