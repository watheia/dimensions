import React from "react"
import { TopLayout } from "./top-layout"

export const Example = () => {
  return (
    <TopLayout>
      <h1>Hello, World!</h1>
    </TopLayout>
  )
}
