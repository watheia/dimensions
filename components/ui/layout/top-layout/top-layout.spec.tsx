import React from "react"
import { render } from "@testing-library/react"
import { expect } from "chai"
import { TopLayout } from "./top-layout"

describe("ui/layout/top-layout", () => {
  it("should render the children", () => {
    const { getByText } = render(
      <TopLayout>
        <h1>Hello, World!</h1>
      </TopLayout>,
    )
    expect(getByText("Hello, World!")).to.exist
  })
})
